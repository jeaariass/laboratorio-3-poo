/*

class Calculador {
    //no se pone ni let ni funtion, da error
    numero1;
    numero2;

    Sumar() {
        let r;
        // this es para decir que es de esta clase, sino se pone da fallo, ya que no se declaro en la funcion 
        r = this.numero2 + this.numero1
        return r;
    }
    Multiplicar() {
        let r;
        r = this.numero1 * this.numero2;
        return r;
    }
}


// la clase va despues de la definicion de la clase 
let miCalculadora = new Calculador();

miCalculadora.numero1 = 8;
miCalculadora.numero2 = 15;

*/
class CalculadorAritmetico {
    numero1;
    numero2;
    Sumar() {
        let r;
        r = this.numero1 - (-this.numero2);
        return r;

    }
    Restar() {
        let r;
        r = this.numero1 - this.numero2;
        return r;

    }
    Modular() {
        let r;
        r = this.numero1 % this.numero2;
        return r;

    }

}

function aritmetico(n) {
    let respuesta;
    let r;
    let nuevoCalculadorAritmetico = new CalculadorAritmetico;

    nuevoCalculadorAritmetico.numero1 = document.getElementById("numero1").value;
    nuevoCalculadorAritmetico.numero2 = document.getElementById("numero2").value;

    if (n == 1) {
        r = nuevoCalculadorAritmetico.Sumar();
        respuesta = r;
    }
    if (n == 2) {
        r = nuevoCalculadorAritmetico.Restar();
        respuesta = r;
    }
    if (n == 3) {
        r = nuevoCalculadorAritmetico.Modular();
        respuesta = r;
    }
    document.getElementById("resultado").value = respuesta;
}
class Calculador {
    numero1;
    numero2;
    Potenciar() {
        let r;
        r = Math.pow(this.numero1, this.numero2);
        return r;
    }
    Logaritmox() {
        let r;
        r = Math.log(this.numero1);
        return r;
    }
    Logaritmoy() {
        let r;
        r = Math.log(this.numero2);
        return r;
    }

}

function calcu(n) {
    let respuesta;
    let r;
    let nuevoCalculador = new Calculador;

    nuevoCalculador.numero1 = document.getElementById("numero1").value;
    nuevoCalculador.numero2 = document.getElementById("numero2").value;

    if (n == 1) {
        r = Number(nuevoCalculador.Potenciar());
        respuesta = r;
    }
    if (n == 2) {
        r = Number(nuevoCalculador.Logaritmox());
        respuesta = r;
    }
    if (n == 3) {
        r = Number(nuevoCalculador.Logaritmoy());
        respuesta = r;
    }
    document.getElementById("resultado").value = respuesta;
}